@extends('tenant.layouts.app')

@section('content')

    <tenant-servicios-index :type-user="{{json_encode(Auth::user()->type)}}"></tenant-servicios-index>

@endsection
